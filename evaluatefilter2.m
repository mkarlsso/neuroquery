function newstruct = evaluatefilter2(cellvar, filterString)
%
% out = evaluatefilter(cellvar, filterString)
%
% Return a cell array the same size as the input cell structure.  Each cell
% contains a matrix where the first column is time and the second column is
% 1's and zeros (true or false).
%
% CELLVAR - any cell structure, for example spikes{}{}{}{}.#### There 
%           must be a time field containing a vector of times. The first output column  
%           is time. All other fields
%           are searcheable, and must be the same length as the time field.
% FILTERSTRING - A string containing an expression with the field variables
%               of CELLVAR.  Each field variable must be preceded with a
%               '$'.
%
% Example: index = evaluatefilter2(linbehave,'(($lindist > 10) && ($velocity > 6))')


newstruct = [];
indexstruct = cellfetch(cellvar, '');

[variables, expression] = parsefilterstring(filterString);

if (size(indexstruct.index,2) == 3) %we are search time segments (i.e., trials)
    epochlist = indexstruct.index(:,1:2);
    epochlist = unique(epochlist,'rows');
    for i = 1:size(epochlist,1)
        newstruct{epochlist(i,1)}{epochlist(i,2)} = [-inf 0; inf 0];
    end
end

for i = 1:size(indexstruct.index,1)
    structVar = [];
    eval(['structVar = cellvar', createcellindex(indexstruct.index(i,:)),';']);
    
    for j = 1:length(variables)
        
        
        
        if (~(isfield(structVar,variables{j}))) 
            error([variables{j},' field does not exist']);
        end
        
        if (~isempty(findstr(expression,'plusminus(')))
            %the user can use the plusminus() function to generate bins
            %around scalar time values.  
            
        
        
        elseif ( (isfield(structVar,'time')) && (size(indexstruct.index,2)==2) )
            %standard time filtering is when the structure has a 'time' field vector
            %and at least one other field of the same length.  For values that
            %pass the condition, the corresponding times pass the filter
            %here the user used a 2-deep structure where the 2nd cell is a
            %structure with the searchable vectors
            %myDataType{session}{epoch}.time[n by 1]
            %myDataType{session}{epoch}.somedataname[n by 1]
            %
            if (size(getfield(structVar,variables{j}),1) ~= size(structVar.time,1))
                error('Filter error: each seach field must be the same length as the time field');
            end
            
         
        elseif (~(isfield(structVar,'timerange') && (size(indexstruct.index,2)==3)))
            %or the user used a 3-deep structure where the third cell is
            %a time period:
            %(myDataType{session}{epoch}{timeBlock})
            %There must be a 'timerange' field with the start and end times
            %of the time block, along with at least one scalar field to
            %search.   
            error('No timerange field found for time filter');
               
        end
        
        
    end
    
    if (~isempty(findstr(expression,'plusminus(')))
        eval(['tmpresult = ',expression,';']);
        
        newstruct{indexstruct.index(i,1)}{indexstruct.index(i,2)} = [newstruct{indexstruct.index(i,1)}{indexstruct.index(i,2)}; tmpresult];
        returntype = 2;
        
        
    elseif ((isfield(structVar,'time')) && (size(indexstruct.index,2)==2) )
        
        eval(['tmpresult = ',expression,';']);
        if (size(tmpresult,1) ~= size(structVar.time,1))
            tmpresult = tmpresult';
        end
        eval(['newstruct', createcellindex(indexstruct.index(i,:)),'= [structVar.time tmpresult];']);
        returntype = 1;
    elseif (isfield(structVar,'timerange') && (size(indexstruct.index,2)==3))
        
        eval(['tmpresult = ',expression,';']);
        if ((tmpresult == 1)||(tmpresult == 0))
            
            %newstruct{indexstruct.index(i,1)}{indexstruct.index(i,2)} = [newstruct{indexstruct.index(i,1)}{indexstruct.index(i,2)}; structVar.timerange(1)-.000000000001 0; structVar.timerange(1) tmpresult; structVar.timerange(2) tmpresult; structVar.timerange(2)+.000000000001 0];
            newstruct{indexstruct.index(i,1)}{indexstruct.index(i,2)} = [newstruct{indexstruct.index(i,1)}{indexstruct.index(i,2)}; structVar.timerange(1) tmpresult; structVar.timerange(2) tmpresult];
            
            
        elseif length(tmpresult > 1)
            error('Time filter error: for timerange searches, the search parameter must have a single scalar output');
        else
            error('Error in time filter.  Make sure syntax is correct and file is properly searchable.');
        end
        
        returntype = 2;
    end
    
  
    
%     for j = 1:length(variables)
%         %standard time filtering is when the structure has a 'time' field vector
%         %and at least one other field of the same length.  For values that
%         %pass the condition, the corresponding times pass the filter
%         %here the user used a 2-deep structure where the 2nd cell is a
%         %structure with the searchable vectors
%         %myDataType{session}{epoch}.time[n by 1]
%         %myDataType{session}{epoch}.somedataname[n by 1]
%         %
%         if ( (isfield(structVar,variables{j})) && (isfield(structVar,'time')) && (size(indexstruct.index,2)==2) ) 
%             if (size(getfield(structVar,variables{j}),1) ~= size(structVar.time,1))
%                 error('Filter error: each seach field must be the same length as the time field');
%             end
%             eval(['tmpresult = ',expression,';']);
%             if (size(tmpresult,1) ~= size(structVar.time,1))
%                 tmpresult = tmpresult';
%             end
%             eval(['newstruct', createcellindex(indexstruct.index(i,:)),'= [structVar.time tmpresult];']);
%             returntype = 1;  
%         elseif (isfield(structVar,variables{j}) && isfield(structVar,'timerange') && (size(indexstruct.index,2)==3))
%             %here the user used a 3-deep structure where the third cell is
%             %a time period:
%             %(myDataType{session}{epoch}{timeBlock})
%             %There must be a 'timerange' field with the start and end times
%             %of the time block, along with at least one scalar field to
%             %search.
%             eval(['tmpresult = ',expression,';']);
%             if ((tmpresult == 1)||(tmpresult == 0))
%                 
%                 %newstruct{indexstruct.index(i,1)}{indexstruct.index(i,2)} = [newstruct{indexstruct.index(i,1)}{indexstruct.index(i,2)}; structVar.timerange(1)-.000000000001 0; structVar.timerange(1) tmpresult; structVar.timerange(2) tmpresult; structVar.timerange(2)+.000000000001 0];
%                 newstruct{indexstruct.index(i,1)}{indexstruct.index(i,2)} = [newstruct{indexstruct.index(i,1)}{indexstruct.index(i,2)}; structVar.timerange(1) tmpresult; structVar.timerange(2) tmpresult];
%            
%             elseif length(tmpresult > 1)
%                 error('Time filter error: for timerange searches, the search parameter must have a single scalar output');
%             else
%                 error('Error in time filter.  Make sure syntax is correct and file is properly searchable.');
%             end
%               
%             returntype = 2;  
%         elseif ~isfield(structVar,variables{j})
%             error([variables{j},' field does not exist'])
%         else
%             error('Error in calling timefilter.  Please make sure syntax is correct and file is properly searchable..');
%         end
%     end
    
end
if (returntype == 2)
    
    for i = 1:size(epochlist,1)
        newstruct{epochlist(i,1)}{epochlist(i,2)} = sortrows(newstruct{epochlist(i,1)}{epochlist(i,2)},1);
    end
end


function out = plusminus(times,pm)
    
    %this creates an include period around each entry in 'times', with exclude tags immediately adjacent. 'pm' is a
    %1 by 2 vector with the [time into past, time into future] window to
    %include.  
    out = [];
    for i=1:length(times)
        out = [out; [times(i)-pm(1)-.0000001 0;times(i)-pm(1) 1;times(i)+pm(2) 1;times(i)+pm(2)+.0000001 0]];
       
    end
    
    

