function out = calcMeanRate(indices, includetimes, spikes, varargin)
% out = calcmeanrate(index, includetimes, spikes, options)
% Calculates the mean rate of a cell for a given epoch as the number of
% spikes/ time.  Only included time periods are used.
%
% Options:
%   'appendindex', 1 or 0 -- set to 1 to append the cell index to the
%   output [tetrode cell value].  Default 0.
%

out = [];  
appendindex = 0;
for option = 1:2:length(varargin)-1   
    if isstr(varargin{option})       
        switch(varargin{option})
            case 'appendindex'
                appendindex = varargin{option+1};
            otherwise
                error(['Option ',varargin{option},' unknown.']);
        end        
    else
        error('Options must be strings, followed by the variable');
    end
end


cellIndex = compileDataInd(indices,2);  %any cell index should have a 2 column index field.
for cellNum = 1:size(cellIndex,1)
    
    index = [indices.epochs cellIndex(cellNum,:)];
    
    
    timeRange = spikes{index(1)}{index(2)}{index(3)}{index(4)}.timerange;
    %timeRange = [0 sessions{index(1)}{index(2)}.duration];
    ontime = diff(timeRange);
    if ~isempty(includetimes)
        
        includetimes(find(includetimes > timeRange(2))) = timeRange(2);
        includetimes(find(includetimes < timeRange(1))) = timeRange(1);
        totalinclude = sum(includetimes(:,2) - includetimes(:,1));
    else
        totalinclude = 0;
    end
    
    totalontime = totalinclude;
    
    
    if ~isempty(spikes{index(1)}{index(2)}{index(3)}{index(4)})
        if ~isempty(spikes{index(1)}{index(2)}{index(3)}{index(4)}.time)
            %Use 'isExcluded' to determine which spikes are excluded in the excludetimes list 
            goodspikes = isIncluded(spikes{index(1)}{index(2)}{index(3)}{index(4)}.time(:,1), includetimes);
        else
            goodspikes = 0;
        end
    else
        goodspikes = nan;
    end
    numgoodspikes = sum(goodspikes);
    
    if (totalontime > 0)
        rate = numgoodspikes/totalontime;
    else
        rate = 0;
    end
    if (appendindex)
        out = [out;[index rate]]; %append the cell index to the value
    else
        out = [out; rate];
    end
end


