function out = combineIncludePeriods(includeA, includeB)
% out = combineIncludePeriods(includeA, includeB)
% Combines the include start and end times from includeA and includeB.
% Each input matrix is N by 2, where the colomns are the start and end
% times for each inclusion period. Only periods that are included in both
% lists are retained.


newIncludePeriods = includeA;

if isempty(includeA)
    out = includeB;
elseif isempty(includeB)
    out = includeA;
else
    for i = 1:size(includeB,1)
        if (isIncluded(includeB(i,1),includeA))
            tmpdist = (includeB(i,1)-includeA(:,1));
            tmpdist(find(tmpdist<0)) = inf;
            [minval, minInd] = min(tmpdist);
            newIncludePeriods(minInd,1) = includeB(i,1);
        end
        if (isIncluded(includeB(i,2),includeA))
            tmpdist = (includeA(:,2)-includeB(i,2));
            tmpdist(find(tmpdist<0)) = inf;
            [minval, minInd] = min(tmpdist);
            newIncludePeriods(minInd,2) = includeB(i,2);
        end
    end
    out = newIncludePeriods;
end


% tmpcombine = [includeA; includeB];
% tmpborder = include2border(tmpcombine);
% out = border2include(tmpborder);
% %------------------------------------------------------
% function out = border2include(borderMatrix)
% 
% statusVector = cumsum(borderMatrix(:,2));
% statusVector(find(statusVector > 0)) = 1;
% 
% startperiods = borderMatrix((find(diff(statusVector) == 1)+1),1);
% endperiods = borderMatrix((find(diff(statusVector) == -1)+1),1);
% 
% out = ([startperiods endperiods]); 
% %------------------------------------------------------
% function out = include2border(includePeriods)
% 
% 
% onVector = includePeriods(:,1);
% onVector(:,2) = 1;
% offVector = includePeriods(:,2);
% offVector(:,2) = -1;
% 
% out = [[-inf 0]; sortrows([offVector;onVector],1)];