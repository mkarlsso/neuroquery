%animals = {'Putin'};
animals = {'putin','nietzsche','mozart','gal'}

epochfilter = [];
epochfilter{1} = {'task','isequal($task,''markov'')'};
epochfilter{2} = {'task','isequal($task,''markov'')'};
datafilter = [];
%datafilter{1} = {'spikes','isequal($area, ''cg1'')','trials','$rewarded = 1'};
%datafilter{2} = {'spikes','isequal($area, ''cg1'')','trials','$rewarded =

%DAta filters can call a custom function that calculates the search terms
%using function arguments
%datafilter{1} = {'<function> getDynamicTrialInfo <argname> trialLag <argval> -3', '($rewarded==1)'};


datafilter{1} = {'spikes','isequal($area, ''cg1'')'};
datafilter{2} = {'spikes','isequal($area, ''cg1'')'};
timefilter = [];

%Filter based on a vector of values. This assumes that there is a 'times'
%field of the same length
%timefilter{1} =  {'pos', '($vel > 1)','trials', '$rewarded == 1'};

%Filter times for entire trial that match the trial search criteria
%timefilter{1} =  {'trials', '$rewarded == 1'};

%We can now filter based on specific timne points, by giving a [before
%after] timerage for each time.
timefilter{1} =  {'trials', 'plusminus($inittypes(end,1),[.25 .25])'};
timefilter{2} =  {'trials', 'plusminus($inittypes(($inittypes(:,2)~=$inittypes(end,2)),1),[.25 .25])'};

%We can also pass a custom function that calculates custom values on the fly
%timefilter{1} = {'<function> get2dstate <argname> immobilecutoff <argval> 1','($immobilitytime > 5)'};


filterfunction = {'calctotalmeanrate',{'spikes'},'appendindex',1};
f = createfilter('animal', animals, 'epochs', epochfilter, 'data', datafilter, 'includetime', timefilter,'function',filterfunction);
f = runfilter(f);

