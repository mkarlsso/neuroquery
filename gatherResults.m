function out = gatherResults(f,analysisFunc)

out = [];

for an = 1:length(f)
    tmpresult = getfield(f(an).output,analysisFunc);
    for group = 1:length(tmpresult.results)
        if (length(out) < group)
            out{group} = [];
        end
        for epoch = 1:length(tmpresult.results{group})
            
            out{group} = [out{group};tmpresult.results{group}{epoch}];
        end
    end
end

