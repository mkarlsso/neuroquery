function out = getTimeSinceLastPulse(animaldir,animalprefix,epochs,varargin)


testarg = [];
for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'testarg'
            testarg = [];
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
end

loaddays = unique(epochs(:,1));
events = loaddatastruct(animaldir, animalprefix, 'events', loaddays);
for i = 1:size(epochs,1)
    
   if isfield(events{epochs(i,1)}{epochs(i,2)},'Laser')
        out{epochs(i,1)}{epochs(i,2)}.time = [events{epochs(i,1)}{epochs(i,2)}.Start:.001:events{epochs(i,1)}{epochs(i,2)}.Stop]';
    
        lastPulseTime =  events{epochs(i,1)}{epochs(i,2)}.Laser(lookup(out{epochs(i,1)}{epochs(i,2)}.time,events{epochs(i,1)}{epochs(i,2)}.Laser,-1));
        timeSinceLastPulse = out{epochs(i,1)}{epochs(i,2)}.time-lastPulseTime;
        timeSinceLastPulse(find(timeSinceLastPulse<0))= nan;    
        out{epochs(i,1)}{epochs(i,2)}.timeSinceLastPulse = timeSinceLastPulse;
   else
       out{epochs(i,1)}{epochs(i,2)}.timeSinceLastPulse = [nan nan]';
       out{epochs(i,1)}{epochs(i,2)}.time = [0 1]';
   end
     
end
%--------------------------------------------------------------------------

