function out = getPulseTimes(animaldir,animalprefix,epochs,varargin)

%out = getPulseTimes(animaldir,animalprefix,epochs,varargin)
%Options:
%   times:  a list of times to sample.  The default is to sample at 1/30
%   outside of pulse times and at .001 during pulses
%timeSinceLastPulse:  measures the time since the end of the last pulse. Zeros indicates that a pulse is currently occuring 
%timeSinceLastPulseStart:  measures the time since the end of the last pulse start
%timeToNextPulseTrain:  measures the time until the next pulse start
%pulseNumber:  counts the pulse number   

times = [];
for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'times'
            times = varargin{option+1};
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
end

loaddays = unique(epochs(:,1));
events = loaddatastruct(animaldir, animalprefix, 'events', loaddays);
for i = 1:size(epochs,1)
    
   if isfield(events{epochs(i,1)}{epochs(i,2)},'Laser')
        firstLaserPulse = events{epochs(i,1)}{epochs(i,2)}.Laser(1,1);
        lastLaserPulse = events{epochs(i,1)}{epochs(i,2)}.LaserOff(end,1);
        if isempty(times)
            %sample densly only during pulse trains 
            out{epochs(i,1)}{epochs(i,2)}.time = [events{epochs(i,1)}{epochs(i,2)}.Start:(1/30):firstLaserPulse ... 
                                                    firstLaserPulse:.001:lastLaserPulse ... 
                                                    lastLaserPulse:(1/30):events{epochs(i,1)}{epochs(i,2)}.Stop]';
            
        else
            out{epochs(i,1)}{epochs(i,2)}.time = times;
        end
        
        %calclate the time relative to the start and end of the last pulse
        lastPulseEndTime =  events{epochs(i,1)}{epochs(i,2)}.LaserOff(lookup(out{epochs(i,1)}{epochs(i,2)}.time,events{epochs(i,1)}{epochs(i,2)}.LaserOff,-1));
        lastPulseStartTime =  events{epochs(i,1)}{epochs(i,2)}.Laser(lookup(out{epochs(i,1)}{epochs(i,2)}.time,events{epochs(i,1)}{epochs(i,2)}.Laser,-1));
        out{epochs(i,1)}{epochs(i,2)}.trainNumber = lookup(out{epochs(i,1)}{epochs(i,2)}.time,events{epochs(i,1)}{epochs(i,2)}.Laser,-1);
        
        
        nextPulseStartTime =  events{epochs(i,1)}{epochs(i,2)}.Laser(lookup(out{epochs(i,1)}{epochs(i,2)}.time,events{epochs(i,1)}{epochs(i,2)}.Laser,1));
        
        timeSinceLastPulseTrain = out{epochs(i,1)}{epochs(i,2)}.time-lastPulseEndTime;
        timeSinceLastPulseTrain(find(timeSinceLastPulseTrain<0))= inf;
        
        %out{epochs(i,1)}{epochs(i,2)}.pulseNumber(find(isnan(timeSinceLastPulseTrain))) = 0;
        
        timeSinceLastPulseTrainStart = out{epochs(i,1)}{epochs(i,2)}.time-lastPulseStartTime;
        timeSinceLastPulseTrainStart(find(timeSinceLastPulseTrainStart<0))= inf;

        timeSinceLastPulseTrain(find((timeSinceLastPulseTrainStart-timeSinceLastPulseTrain) < 0)) = 0;
        
        timeToNextPulseTrain = nextPulseStartTime - out{epochs(i,1)}{epochs(i,2)}.time;       
        timeToNextPulseTrain(find(timeToNextPulseTrain<0))= nan;  
        
        out{epochs(i,1)}{epochs(i,2)}.timeSinceLastPulse = timeSinceLastPulseTrain;  %measures the time since the end of the last pulse
        out{epochs(i,1)}{epochs(i,2)}.timeSinceLastPulseStart = timeSinceLastPulseTrainStart; %measures the time since the end of the last pulse start
        out{epochs(i,1)}{epochs(i,2)}.timeToNextPulseStart = timeToNextPulseTrain; %measures the time until the next pulse start
        
   else
       out{epochs(i,1)}{epochs(i,2)}.time = [0 1]';
       out{epochs(i,1)}{epochs(i,2)}.pulseNumber = [nan nan]';
       out{epochs(i,1)}{epochs(i,2)}.timeSinceLastPulse = [nan nan]';
       out{epochs(i,1)}{epochs(i,2)}.timeSinceLastPulseStart = [nan nan]';
       out{epochs(i,1)}{epochs(i,2)}.timeToNextPulseStart = [nan nan]';
           
   end

end
%--------------------------------------------------------------------------

