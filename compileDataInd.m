function ind = compileDataInd(indices,numColumns)

%ind = compileDataInd(indices,numColumns)
%To be used inside an analysis function to determine the combined data
%indices. If more than one field in 'indices' matches the desired column
%count, then the intersection of those indices is returned.
%indices -- the 'indices' variable that is passed to the analysis function
%numColumns -- only fields with this number of columns are included 


fields = fieldnames(indices);
ind = nan;
for i = 1:length(fields)
    if (~isequal(fields{i},'epochs') && ~isequal(fields{i},'animal'))
        tmpind = getfield(indices,fields{i});
        if (size(tmpind,2) == numColumns) %the number of columns must match numColumns
            if isnan(ind)
                ind = tmpind;
            else
                ind = intersect(ind,tmpind,'rows'); 
            end
        end
    end
end

            
        