function out = getLaserResponses(animaldir,animalprefix,epochs,varargin)

%out = getPulseTimes(animaldir,animalprefix,epochs,varargin)
%Options:
 

binSize = .005;
baseLineWindow = .500;
changeThresholdHigh = 95;


for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'binSize'
            binSize = varargin{option+1};
        case 'baseLineWindow'
            baseLineWindow = varargin{option+1};
        case 'changeThresholdHigh'
            changeThresholdHigh = varargin{option+1};
       
         
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
end

loaddays = unique(epochs(:,1));
events = loaddatastruct(animaldir, animalprefix, 'events', loaddays);
spikes = loaddatastruct(animaldir, animalprefix, 'spikes', loaddays);
for epInd = 1:size(epochs,1)

    if isfield(events{epochs(epInd,1)}{epochs(epInd,2)},'Laser')
        firstLaserPulse = events{epochs(epInd,1)}{epochs(epInd,2)}.Laser(1,1);
        lastLaserPulse = events{epochs(epInd,1)}{epochs(epInd,2)}.LaserOff(end,1);

        %start and end of each laser pulse
        laserTimes = [events{epochs(epInd,1)}{epochs(epInd,2)}.Laser  events{epochs(epInd,1)}{epochs(epInd,2)}.LaserOff];
        laserDuration = laserTimes(1,2)-laserTimes(1,1);
                 
        %iterate though every cell, starting with cell 2 (to exclude
        %multiunit)
        for electrodeInd = 1:length(spikes{epochs(epInd,1)}{epochs(epInd,2)})
            for cellInd = 2:length(spikes{epochs(epInd,1)}{epochs(epInd,2)}{electrodeInd})


                tmpspikes = spikes{epochs(epInd,1)}{epochs(epInd,2)}{electrodeInd}{cellInd}.time;
                spikeIndDuringLaser = find(isExcluded(tmpspikes, laserTimes));   %calculates if the spike times are INSIDE the laser times
                %spikeIndOutsideLaser = find(tmpspikes < firstLaserPulse)
                
                %Calculates the time between the individual spike and the
                %next laser start time
                nextPulseStartTime =  laserTimes(lookup(tmpspikes,laserTimes(:,1),1),1);
                
                
                
               

                %calculates the time of the end of the last laser pulse
                lastPulseEndTime =  laserTimes(lookup(tmpspikes,laserTimes(:,2),-1),2);
                %calculates the time of the start of the last laser pulse
                lastPulseStartTime = laserTimes(lookup(tmpspikes,laserTimes(:,1),-1),1);
                lastPulseNumber = lookup(tmpspikes,laserTimes(:,1),-1);

                %calculates the time between the spike and the start of the
                %last laser pulse
                timeSinceLastPulseStart = tmpspikes-lastPulseStartTime;
                
                %copes with the first laser pulse event
                timeSinceLastPulseStart(find(timeSinceLastPulseStart<0))= nan;

                %timeSinceLastPulseEnd(find((timeSinceLastPulseTrainStart-timeSinceLastPulseTrain) < 0)) = 0;

                %calculates the time between the spike and the start of the
                %next laser pulse
                timeToNextPulseStart = nextPulseStartTime - tmpspikes;
                %deals with the last laser pulse event
                timeToNextPulseStart(find(timeToNextPulseStart<0))= nan;
                
                %find the spike indices of spikes that occurred during the baseline period
                baseLineSpikeInd =  find(timeToNextPulseStart < baseLineWindow);
                %calculates total#spikes in baseline/time of baseline
                %period
                baselineFR = length(baseLineSpikeInd)/(baseLineWindow*size(laserTimes,1));
                %calculates total#spikes in laser/time of laser pulse
                laserFR = length(spikeIndDuringLaser)/(laserDuration*size(laserTimes,1));
                
                %creates histogram showing distribution of spikes during
                %laser pulse and next 10 msec, divided into bins of binSize width
                LaserSpikesHistogram=histc(timeSinceLastPulseStart,[0:binSize:(laserDuration+.01)])/max(lastPulseNumber);
                %creates histogram showing distribution of spikes during
                %baseline period (defined as BaseLineWindow), divided into
                %bins of binSize width
                BaselineSpikesHistogram=histc(timeToNextPulseStart,[0:binSize:baseLineWindow])/max(lastPulseNumber);
                
                %binMatrix = hist2d([lastPulseNumber timeToNextPulseStart],0:100:max(lastPulseNumber),0:binSize:baseLineWindow)/100;
                %baseLineSpikeCountConfidenceLimits = prctile(binMatrix(:),[5 95]);               
                %baseLineMeanSpikeCount = mean(binMatrix(:));
                
                %calculates 95% confidence interval of # spikes/bin during
                %BaseLine period  
                baseLineSpikeCountConfidenceLimits = prctile(BaselineSpikesHistogram,[5 changeThresholdHigh]);
                
                SigBinIndices = find(LaserSpikesHistogram>=baseLineSpikeCountConfidenceLimits(2));
                if isempty(SigBinIndices)
                    LatencyToFirstSigBin = inf;
                else
                    LatencyToFirstSigBin=(SigBinIndices(1)-1)*(binSize);
                end
                
                NumSigBins=length(SigBinIndices);
                
              
                
%                 if (laserFR/baselineFR > 5)
%                 keyboard
%                 end

                %out{epochs(epInd,1)}{epochs(epInd)}{electrodeInd}{cellInd}.latencyToFirstSigBin = [];
                out{epochs(epInd,1)}{epochs(epInd,2)}{electrodeInd}{cellInd}.averaqeRateDuringLaser = laserFR;
                out{epochs(epInd,1)}{epochs(epInd,2)}{electrodeInd}{cellInd}.baseLineRate = baselineFR;
                out{epochs(epInd,1)}{epochs(epInd,2)}{electrodeInd}{cellInd}.LatencyToFirstSigBin = LatencyToFirstSigBin;
                out{epochs(epInd,1)}{epochs(epInd,2)}{electrodeInd}{cellInd}.NumSigBins = NumSigBins;
            end
        end
    else %no laser field was present for this epoch
        %out{epochs(epInd,1)}{epochs(epInd)}{electrodeInd}{cellInd}.latencyToFirstSigBin = [];
        
        out{epochs(epInd,1)}{epochs(epInd,2)} = [];
    end
end


   
  

%--------------------------------------------------------------------------

