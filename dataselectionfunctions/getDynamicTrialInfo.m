function out = getDyamicTrialInfo(animaldir,animalprefix,epochs,varargin)

%out = getDyamicTrialInfo(animaldir,animalprefix,epochs,options)
%An example of how to calculate trial information on the fly to use as a
%data filter.
%required inputs are animaldir,animalprefix,and epochs.
%out-- a 3-deep cell structure (out{day}{epoch}{trialnum}.XX) that contains
%the new searrable data.

%Options:
 
trialLag = 0;

for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'trialLag'
            trialLag = varargin{option+1};
                 
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
end

loaddays = unique(epochs(:,1));
trials = loaddatastruct(animaldir, animalprefix, 'trials', loaddays);

for epInd = 1:size(epochs,1)

    for trialNum = 1:length(trials{epochs(epInd,1)}{epochs(epInd,2)})
        if ((trialLag < 0) && (trialNum > abs(trialLag)))|| ((trialLag >= 0) && (trialNum < length(trials{epochs(epInd,1)}{epochs(epInd,2)})-trialLag))
            out{epochs(epInd,1)}{epochs(epInd,2)}{trialNum}.rewarded = trials{epochs(epInd,1)}{epochs(epInd,2)}{trialNum+trialLag}.rewarded;
        else
            out{epochs(epInd,1)}{epochs(epInd,2)}{trialNum}.rewarded = nan;
        end
    end
end

    


   
  

%--------------------------------------------------------------------------

