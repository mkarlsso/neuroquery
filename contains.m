function out = contains(str1,str2)
%out = contains(str1,str2)
%A wildcard string search function.  Searches str1 for the pattern
%indicated in str2.  str2 can contain '*', which means 'any character'.
%Example:
%contains('hi this is my name','*this*')
%
%ans = 1


if (~ischar(str1) || ~ischar(str2))
    error('Both inputs must be strings');
end

str2 = ['^',str2];
str2 = regexprep(str2,'*','.*');
ind = regexp(str1,str2);

if (~isempty(ind))
    out = 1;
else
    out = 0;
end





